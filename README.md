# osbf

The osbf python program is a brute force tool used to crack passwords.

<span style="color:red">Specifically developed for the macOS platform.</span>

The program supports the following formats in particular :

- [DMG](https://en.wikipedia.org/wiki/Apple_Disk_Image) 
- [ZIP](https://fr.wikipedia.org/wiki/ZIP_(format_de_fichier))
- [SUDO](https://fr.wikipedia.org/wiki/Sudo)


```diff
- The program is compatible with the Python 3 version only.
```
```diff
- Some special dictionary characters are not supported.
```


## Requirements

The osbf tool uses certain dependencies to be installed beforehand in order to ensure that it works properly.

The Python 3.9.6 interpreter must be present on the system.

* [Python](https://www.python.org/)




## Installation standard


To install the latest development version, the following commands are required :

```bash

# 1. Download the latest version of the tool.

# 2. Unzip the obtained tool.


Latest version : https://gitlab.com/osxcode/osbf/osbf


```
 

## Use

The program can be launched using the main.py file :

```bash
python3 main.py

```

-- 

Once executed, the program launches a minimalist graphical interfaces allowing to perform brute force attacks on different targets.

![alt text](md_data/img/osbf.png)


## Tests

#### Hardware / Software

<span style="color:red">Information - The tests as well as the development of the tool have been carried out with the versions of the following tools and systems: </span>

- Python 3.9.6

- Target system for data reception : macOS 10.15.7 (19H114)




## Maintainers

Current maintainer :

* osxcode - [https://gitlab.com/osxcode/osbf](https://gitlab.com/osxcode/osbf)




## Further development

- Support for special characters of some dictionary.
- Multithreading management.
- Support for new targets.




## Privacy

<span style="color:red">The application osbf does not collect or store any personal data about the user.</span>




## Warning

Using osbf outside of personal activity or without prior authorization in infrastructures may be considered illegal activity. 
It is the end user's responsibility to obey all applicable local, state and federal laws.  
The author is in no way responsible for any misuse or damage caused by this program.




## Support

If you want you can support me ;)

[-- OXEN --](https://oxen.io)


<img src="https://osxcode.gitlab.io/os/img/QRCODE_OXEN_WALLET.png" width="500" height="350">




## Licence

[LGPL - Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html)
