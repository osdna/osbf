# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   functions_menu.py                                                        *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *


import os
import tkinter
import time

from tkinter import ttk
from tkinter import font

 
def exit_osbf(root):
	if messagebox.askokcancel("Exit", "Do you really want to exit ?"):
		root.destroy()
		exit()  

def licence(event=None):
	os.system("open https://gitlab.com/osxcode/osbf#licence")

def contact_support(event=None):
	os.system("open https://gitlab.com/osxcode/osbf/osbf/-/blob/main/README.md")

def support_us(event=None):
	os.system("open https://gitlab.com/osxcode/osbf#support")

def confidentiality(event=None):
	os.system("open https://gitlab.com/osxcode/osbf#privacy")


def about_osbf():
	win_About = tkinter.Toplevel()
	win_About.geometry("380x500")
	win_About.config(bg="#252526")

	win_About.title("About OSBF")
	win_About.resizable(width=False, height=False)

	logo = tkinter.PhotoImage(file="../data/img/AppIcon.png")
	label_logo = tkinter.Label(win_About, image=logo, bg="#252526")

	font_title = font.Font(win_About, size=18, weight='bold', family='Helvectica')
	font_txt = font.Font(win_About, size=12, family='Helvectica')

	label_title = tkinter.Label(win_About, text="OSBF", bg="#252526", font=font_title)
	label_title.place(relx=.5, rely=.1, anchor="center")

	label_version = tkinter.Label(win_About, text="Version 1.0", bg="#252526", font=font_txt)
	label_version.place(relx=.5, rely=.20, anchor="center")

	label_licence = tkinter.Label(win_About, text="OSXCODE", bg="#252526", font=font_txt)
	label_licence.place(relx=.5, rely=.93, anchor="center")
	label_licence.bind("<Button-1>",contact_support)

	progress_bar = tkinter.ttk.Progressbar(win_About, orient="horizontal", length=100, mode='determinate')
	progress_bar.place(relx=0.5, rely=0.52, anchor="center")
	progress_bar["value"] = 0
	progress_bar["maximum"] = 100

	for i in range(100):
		progress_bar["value"] += 1
		progress_bar.update()
		time.sleep(0.01)
	progress_bar.destroy()

	label_logo.place(relx=.5, rely=.52, anchor="center")
	win_About.wm_attributes('-alpha', 0.96) 
	win_About.mainloop()
