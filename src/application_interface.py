# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   application_interface.py                                                 *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *


import os

import tkinter
from tkinter import ttk
from tkinter import font

from env import *
from osbf import *


class Application(tkinter.Frame):    

	def __init__(self, master=None):
		tkinter.Frame.__init__(self, master)
		master.geometry("660x300")
		master.config(bg="#252526")
	
		police_saisie = tkinter.font.Font(master, size=24, weight='bold', family='Courier')
		police_scanf_time = font.Font(master, size=16, weight='bold', family='Courier')

		if (os.sys.platform == 'darwin'):
			police_bu = font.Font(master, size=14, weight='bold', family='Courier')
		else:
			police_bu = font.Font(master, size=10, weight='bold', family='Courier')

		#
		# Creation of display labels
		#

		self.label_target_name = tkinter.Text(master, wrap=tkinter.NONE)
		self.label_target_name.place(relx=.60, rely=.50, anchor="center", height="25", width="400")
		self.label_target_name.config(highlightbackground="#a7a7a7", state='disabled')

		self.label_dictionary_name = tkinter.Text(master, wrap=tkinter.NONE)
		self.label_dictionary_name.place(relx=.60, rely=.65, anchor="center", height="25", width="400")
		self.label_dictionary_name.config(highlightbackground="#a7a7a7", state='disabled')

		#
		# Select the target file
		#

		self.browse_target = tkinter.Button(master, text="  Browse Target  ", command=lambda: self.browse_file(self), font=police_bu)
		self.browse_target.place(relx=.15, rely=.5, anchor="center")
		self.browse_target.config(highlightbackground="#252526")

		#
		# Select the target dictionary
		#

		self.browse_dictionary = tkinter.Button(master, text="Browse Dictionary", command=lambda: self.browse_dico(self), font=police_bu)
		self.browse_dictionary.place(relx=.15, rely=.65, anchor="center")
		self.browse_dictionary.config(highlightbackground="#252526")

		#
		# Select the target dictionary
		#

		self.bf_sudo = tkinter.Button(self.master, text="BF_SUDO", command=lambda: self.browse_sudo(self), fg = "red")
		self.bf_sudo.place(relx=.15, rely=.80, anchor="center")
		self.bf_sudo.config(highlightbackground="#252526")

		#
		# Creation of the validation button
		#

		self.validation = tkinter.Button(master, text="  OK  ", command=lambda: self.go(self), font=police_bu)
		self.validation.place(relx=.80, rely=.85, anchor="center")
		self.validation.config(highlightbackground="#252526")
		self.validation.config(state='normal')

		#
		# Creation of the progress bar
		#

		self.style_progress_bar = ttk.Style()
		self.style_progress_bar.theme_use('clam')
		self.style_progress_bar.configure("red.Horizontal.TProgressbar", foreground='red', background='red')
		self.progress_bar = ttk.Progressbar(master, style="red.Horizontal.TProgressbar", orient="horizontal", length=400, mode='determinate')
		self.progress_bar.place(relx=.60, rely=0.25, anchor="center")
		self.progress_bar["value"] = 0
		self.progress_bar["maximum"] = 100

		#
		# Interrupt attack button
		#

		self.stop = tkinter.Button(master, text=" Cancel / Restart  ", command=lambda: self.end(self), font=police_bu)
		self.stop.place(relx=.48, rely=.85, anchor="center")
		self.stop.config(highlightbackground="#252526")
		self.stop.config(state='normal')

		#
		# Logo creation
		#

		canva = tkinter.Canvas(master, width=90, height=90)
		canva.config(bg='#252526', highlightbackground='#252526')
		canva.pack()
		canva.place(relx=0.13, rely=0.2, anchor="center")
		id_text = canva.create_text(60, 60, font=("Purisa", 70), text='O', fill = 'red')

	#
	# Function allowing the selection of the target with brute force
	#

	def browse_file(self, event):
		global patch_file
		patch_file = tkinter.filedialog.askopenfilename()
		patch_file =  " \"" + patch_file + "\""
		global fileExtension
		fileName, fileExtension = os.path.splitext(patch_file)

		#
		# Checking the validity of the selected file.
		#

		if patch_file == ' ""':
			self.label_target_name.config(state='normal')
			self.label_target_name.delete("1.0", tkinter.END)
			self.label_target_name.config(state='disabled')
			self.label_target_name.config(background="#FE6969")
		else:
			self.label_target_name.config(state='normal')
			self.label_target_name.delete("1.0", tkinter.END)
			self.label_target_name.insert('1.0', patch_file)
			self.label_target_name.config(background="#A3FEA3", state='disabled')

	#
	# Function allowing the selection of the dictionary file
	#

	def browse_dico(self, event):
		global patch_dico
		patch_dico = tkinter.filedialog.askopenfilename()
		 
		#
		# Checking the validity of the selected file
		#

		if patch_dico == ' ""':
			self.label_dictionary_name.config(state='normal')
			self.label_dictionary_name.delete("1.0", tkinter.END)
			self.label_dictionary_name.config(state='disabled')
			self.label_dictionary_name.config(background="#FE6969")
		else:
			self.label_dictionary_name.config(state='normal')
			self.label_dictionary_name.delete("1.0", tkinter.END)
			self.label_dictionary_name.insert('1.0', " \"" + patch_dico + "\"")
			self.label_dictionary_name.config(background="#A3FEA3", state='disabled')
		   
			if (round(os.path.getsize(patch_dico) / (1024 * 1024), 3) < 100):
				global num_lines
				num_lines = 0
				with open(patch_dico, 'r',  encoding = "ISO-8859-1") as f:
					for line in f:
						num_lines += 1
				print("Number of lines : ")
				print(num_lines)
			else:
				num_lines = 0

	def browse_sudo(self, event):
		global fileExtension
		fileExtension = "d338b3f0f405eb5e51c8cc1e5ca66f02"
		self.label_target_name.config(state='normal')
		self.label_target_name.delete("1.0", tkinter.END)
		self.label_target_name.config(state='disabled')
		self.label_target_name.config(background="#252526")
		self.browse_dico(event)

	def go(self, event):
		self.bf_sudo.config(state='disabled') 
		self.validation.config(state='disabled') 
		self.browse_target.config(state='disabled')  
		self.browse_dictionary.config(state='disabled')  
		self.entra = Oscodebf(patch_dico, num_lines,
		self.progress_bar, self.style_progress_bar, fileExtension, patch_file)

		self.entra.setDaemon(True)		
		self.entra.start()
		
	def end(self, event):

		self.entra.stop()
		self.progress_bar["value"] = 0
		self.progress_bar.configure(style="red.Horizontal.TProgressbar")
		self.bf_sudo.config(state='normal')  
		self.validation.config(state='normal')  
		self.browse_target.config(state='normal')  
		self.browse_dictionary.config(state='normal') 
