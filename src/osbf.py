# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   osbf.py                                                            *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *


import os
import threading

from env import *


global patch_file


class Oscodebf(threading.Thread):

	def __init__(self, patch_dico, num_lines, progress_bar, style_progress_bar, fileExtension, patch_file):
		threading.Thread.__init__(self)
		self.state = True
		self.patch_dico = patch_dico
		self.num_lines = num_lines
		self.progress_bar = progress_bar
		self.style_progress_bar = style_progress_bar
		self.fileExtension = fileExtension
		self.patch_file = patch_file

	def run(self):  
		number_lines_file = 0
		with open(self.patch_dico, 'r', encoding = "ISO-8859-1") as data_file:
			for line in data_file:
				if (self.num_lines > 0):
					number_lines_file += 1
					self.progress_bar["value"] = (number_lines_file/self.num_lines)*100
					
				if self.state == False:
					break

				if self.fileExtension.lower() == '.zip"':
					system_output = os.system("unzip -P '" + line.rstrip() + "' "  + self.patch_file)

				if self.fileExtension.lower() == '.dmg"':
					system_output = os.system("echo 'echo -n " + line.rstrip() + " | hdiutil attach -stdinpass " + self.patch_file + "' | bash")

				if self.fileExtension.lower() == 'd338b3f0f405eb5e51c8cc1e5ca66f02':
					system_output = os.system("echo 'echo -n " + line.rstrip() + " | sudo -kS ls " + "' | bash")
 
				if system_output == 0:
					self.state = False
					self.style_progress_bar.configure("green.Horizontal.TProgressbar", foreground='green', background='green')
					self.progress_bar.configure(style="green.Horizontal.TProgressbar")
					new_days = open(os.getcwd() + '/result.txt','w')
					new_days.write("Password : " + line)
					new_days.close()

	def stop(self): 
		self.state = False
