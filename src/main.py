# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   main.py                                                                  *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *


import os
import sys
import time
import hashlib
import threading


from application_interface import *
from menu import *


if __name__ == "__main__":
	
	from window_def import *

	owd = os.getcwd()
	os.chdir(owd)

	#
	# Definition of the display window
	#

	window_os_bf = tkinter.Tk()
	window_os_bf.title("OSBF")
	window_os_bf.resizable(width=False, height=False)

	window_center(window_os_bf)
	menu(window_os_bf, "OSBF")

	app = Application(window_os_bf)
	app.config(bg="#CECECE")

	window_os_bf.wm_attributes('-alpha', 0.98, '-topmost', 1)
	window_os_bf.mainloop()
