# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   menu.py                                                                  *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *



import tkinter as tk

from functions_menu import * 

def menu(root, title):

	menubarre = tk.Menu(root)

	menu1 = tk.Menu(menubarre, tearoff=0)
	menu1.add_command(label="About OSBF", command = lambda:about_osbf())
	menu1.add_command(label="Licence", command = lambda: licence())
	menu1.add_command(label="Contact support", command=lambda: contact_support())
	menu1.add_separator()
	menu1.add_command(label="Support us", command=lambda:support_us())
	menu1.add_command(label="Confidentiality", command=lambda: confidentiality())
	menu1.add_separator()
	menu1.add_command(label="Exit", command=lambda: exit_osxcodebf(root))
	menubarre.add_cascade(label=title, menu=menu1)
 
	root.config(menu=menubarre)
