# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   window_def.py                                                            *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *



from tkinter import filedialog

#
# Functions for centering the window
#


def geoliste(geo):
	r = [i for i in range(0, len(geo)) if not geo[i].isdigit()]
	return [int(geo[0:r[0]]), int(geo[r[0] + 1:r[1]]), int(geo[r[1] + 1:r[2]]), int(geo[r[2] + 1:])]


def window_center(fen):
	fen.update_idletasks()
	l, h, x, y = geoliste(fen.geometry())
	fen.geometry("%dx%d%+d%+d" % (l, h, (fen.winfo_screenwidth() - l) // 2, (fen.winfo_screenheight() - h) // 2))
